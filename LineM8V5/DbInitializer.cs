﻿using LineM8V5.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LineM8V5
{
    public class DbInitializer
    {
        public static void Initialize(LineM8V5DbContext context)
        {
            context.Database.EnsureCreated();

            // Look for any AircraftIdentities.
            if (context.AircraftIdentities.Any())
            {
                return;   // DB has been seeded
            }

            //Aircraft Identities
            var aircraftIdentities = new AircraftIdentity[]
            {
            new AircraftIdentity{AircraftMSN="123456"},
            };
            foreach (AircraftIdentity a in aircraftIdentities)
            {
                context.AircraftIdentities.Add(a);
            }
            context.SaveChanges();

            //Aircraft Types
            var aircraftTypes = new AircraftType[]
            {
            new AircraftType{AircraftTypeName="A330-300"},
            };
            foreach (AircraftType t in aircraftTypes)
            {
                context.AircraftTypes.Add(t);
            }
            context.SaveChanges();

            //Airlines
            var airlines = new Airline[]
            {
            new Airline{AirlineName="Cathay Pacific Airways"},
            };
            foreach (Airline a in airlines)
            {
                context.Airlines.Add(a);
            }
            context.SaveChanges();

            //Engine Types
            var engineTypes = new EngineType[]
{
            new EngineType{EngineTypeName="RR T700"},
};
            foreach (EngineType e in engineTypes)
            {
                context.EngineTypes.Add(e);
            }
            context.SaveChanges();

        }
    }
}
