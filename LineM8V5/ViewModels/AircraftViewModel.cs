﻿using LineM8V5.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LineM8V5.ViewModels
{
    public class AircraftViewModel
    {
        [Display(Name = "Aircraft MSN")]
        [StringLength(10)]
        [RegularExpression(@"^[A-Z0-9-]*$")]
        public string AircraftMSN { get; set; }

        //[Required]
        [Display(Name = "Aircraft Type")]
        public IEnumerable<AircraftType> AircraftType { get; set; }

        //[Required]
        [Display(Name = "Aircraft Engine Type")]
        public IEnumerable<EngineType> EngineType { get; set; }

        //[Required]
        [Display(Name = "Airline Name")]
        public IEnumerable<Airline> Airline { get; set; }

        //[Required]
        [RegularExpression(@"^[A-Z0-9-]*$")]
        [StringLength(10, MinimumLength = 3)]
        [Display(Name = "Aircraft Registration")]
        public string AircraftRegistration { get; set; }

        public string SearchQuery { get; set; }
    }
}
