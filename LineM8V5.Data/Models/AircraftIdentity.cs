﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LineM8V5.Data.Models
{
    public class AircraftIdentity : BaseEntity
    {
        public int AircraftId { get; set; }
        public string AircraftMSN { get; set; }
        public IEnumerable<AircraftType> AircraftType { get; set; }
        public IEnumerable<EngineType> EngineType { get; set; }

        public Aircraft Aircraft { get; set; }
    }
}
