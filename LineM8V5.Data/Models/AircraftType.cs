﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LineM8V5.Data.Models
{
    public class AircraftType : BaseEntity
    {
        public int AircraftTypeId { get; set; }
        public string AircraftTypeName { get; set; }

        public AircraftIdentity AircraftIdentity { get; set; }
    }
}
