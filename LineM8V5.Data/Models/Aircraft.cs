﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LineM8V5.Data.Models
{
    public class Aircraft : BaseEntity
    {

        public int AircraftId { get; set; }
        public IEnumerable<AircraftIdentity> AircraftIdentities { get; set; }
        public IEnumerable<Airline> Airline { get; set; }
        public string AircraftRegistration { get; set; }
    }
}
