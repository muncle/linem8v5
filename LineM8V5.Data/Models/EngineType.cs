﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LineM8V5.Data.Models
{
    public class EngineType : BaseEntity
    {
        public int EngineTypeId { get; set; }
        public string EngineTypeName { get; set; }

        public AircraftIdentity AircraftIdentity { get; set; }
    }
}
