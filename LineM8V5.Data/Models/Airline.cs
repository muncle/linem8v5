﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LineM8V5.Data.Models
{
    public class Airline : BaseEntity
    {
        public int AirlineId { get; set; }
        public string AirlineName { get; set; }

        public Aircraft Aircraft { get; set; }
    }
}
